python-scientific (2.9.4-4) UNRELEASED; urgency=medium

  * update the Vcs-Git and bump Standards-Version
  * add the autopkgtest suite

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 01 Jan 2015 13:37:33 +0100

python-scientific (2.9.4-3) unstable; urgency=medium

  * revert experimental changes
  * build openmpipython on arm64 and ppc64el (Closes: #746697)
    thanks to Logan Rosen for the patch

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 02 Aug 2014 22:59:46 +0200

python-scientific (2.9.4-3~experimental1) experimental; urgency=medium

  * try to get rid of the arch specific part

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 02 Aug 2014 10:35:28 +0200

python-scientific (2.9.4-2) unstable; urgency=medium

  * Team upload.
  * [2951213] Replace libmpich2-dev by limbpich-dev.

 -- Anton Gladky <gladk@debian.org>  Thu, 08 May 2014 21:59:50 +0200

python-scientific (2.9.4-1) unstable; urgency=medium

  * Imported Upstream version 2.9.4

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 24 Jan 2014 18:23:27 +0100

python-scientific (2.9.3-2) unstable; urgency=medium

  * do not install the _netcdf.so extension in python-scientific
    (Closes: #736468) thanks to Yannick Roehlly

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 24 Jan 2014 07:54:45 +0100

python-scientific (2.9.3-1) unstable; urgency=medium

  * Imported Upstream version 2.9.3
  * Bump Standards-Version 3.9.5 (nothing to do)
  * Build-Depends on cython and use the .pyx files instead of the
    pre-compiled .c extensions.
  * Execute the test suite during the build
  * debian/patches
    - deleted (applyed by upstream)
      - 0001-replace-PyObject_NEW-Pyobject_new.patch
      - 0003-feature-forwarded-numpy-arrayobject-header.patch
    - renamed
      - 0002-debian-deal-with-mpi-compilers.patch
        -> 0001-debian-deal-with-mpi-compilers.patch

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 23 Jan 2014 13:15:39 +0100

python-scientific (2.9.2-5) unstable; urgency=low

  * handled the link to directory conversion done with the
    last upload of mpich2python and openmpipython documentation
    (Closes: #720150)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 19 Aug 2013 16:26:00 +0200

python-scientific (2.9.2-4) unstable; urgency=low

  * debian/rules clean a few file to build twice without problem.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 12 May 2013 11:37:16 +0200

python-scientific (2.9.2-3) unstable; urgency=low

  * Uploaded to unstable

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 12 May 2013 08:32:07 +0200

python-scientific (2.9.2-2) experimental; urgency=low

  * fix the FTBFS when building only the arch packages.
    do not install the upstream changelog in that case.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 21 Apr 2013 17:52:17 +0200

python-scientific (2.9.2-1) experimental; urgency=low

  * Imported Upstream version 2.9.2
    (Closes: #263254, #270930, #350109, #379745, #458034, #491538, #651502)
  * Maintained under Debian-Science umbrella (Closes: #624748)
  * Switch to source format 3.0 (quilt)
  * Use dh instead of plain debhelper
  * Switch to compat level 9
  * Bump Standards-Version 3.9.4 (nothing to do)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 20 Jan 2013 17:01:32 +0200

python-scientific (2.8-4) unstable; urgency=low

  * QA upload.
  * Use correct python platform value on kfreebsd (closes: #692281).
  * Build-depend and depend python-numpy >= 1:1.6.1-1 due to ABI change
    between numpy 1.5 and 1.6 (closes: #692318).

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 04 Nov 2012 22:52:37 +0000

python-scientific (2.8-3) unstable; urgency=low

  * QA upload.
  * Replace EOLed MPI implementations (LAM, MPICH1) with actively maintained
    implementations (OpenMPI, MPICH2).  (Closes: #571452)
  * Break python-scientific/python-netcdf circular dependency by changing p-s
    to Recommend p-n.

 -- Nicholas Breen <nbreen@ofb.net>  Tue, 10 May 2011 14:33:35 -0700

python-scientific (2.8-2) unstable; urgency=low

  * Orphan the package.
  * Build using dh_python2. Closes: #617027.

 -- Matthias Klose <doko@debian.org>  Sun, 01 May 2011 10:36:03 +0200

python-scientific (2.8-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Add a recommends on pyro.  Recommends, as most of python-scientific is
    usable without it.  Only DistributedComputing depends on it.
    (Closes: #567864)
  * Rename /usr/bin/task_manager to /usr/bin/task_manager_pyscientific.
    (Closes: #547511)

 -- Richard Darst <rkd@zgib.net>  Sun, 31 Jan 2010 16:10:34 -0500

python-scientific (2.8-1.1) unstable; urgency=low

  * Non-maintainer upload.
    - Applied Kumar's patch for fixes below and uploaded it.
  * Fix "manipulates site-packages/ directly, failing with Python 2.6"
    (Closes: #547852)
  * Fix "FTBFS due to netcdfg-dev not being available anymore"
    (Closes: #549477)

 -- Bastian Venthur <venthur@debian.org>  Sat, 07 Nov 2009 13:11:03 +0100

python-scientific (2.8-1) unstable; urgency=low

  * New upstream version.
  * Build against NumPY. Closes: #478460.
  * Complete sentences in package descriptions. Closes: #475997, #475996,
    #475999.

 -- Matthias Klose <doko@debian.org>  Mon, 14 Sep 2009 23:37:22 +0200

python-scientific (2.4.11-2) unstable; urgency=low

  * Rebuild to move files to /usr/share/pyshared. Closes: #490498, #490500.
  * Update watch file. Closes: #449764.

 -- Matthias Klose <doko@debian.org>  Sat, 12 Jul 2008 13:37:40 +0200

python-scientific (2.4.11-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix use of Python API memory handling functions.
    Closes: #468999

 -- Thomas Viehmann <tv@beamnet.de>  Wed, 05 Mar 2008 18:29:30 +0100

python-scientific (2.4.11-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * use source:Version instead of Source-Version where appropriate
    closes: 465685
  * enforce gfortran version of python-numeric-ext
  * add python-central build-dep

 -- Riku Voipio <riku.voipio@iki.fi>  Wed, 27 Feb 2008 01:12:49 +0200

python-scientific (2.4.11-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun, 22 Oct 2006 02:09:13 +0000

python-scientific (2.4.9-6) unstable; urgency=low

  * Fix python-scientific / python-netcdf dependency. Closes: #374801.

 -- Matthias Klose <doko@debian.org>  Wed, 21 Jun 2006 19:25:28 +0000

python-scientific (2.4.9-5) unstable; urgency=low

  * Convert to the updated Python policy. Closes: #373351.
    ... makes Scientific available for different python versions.
    Closes: #319384.

 -- Matthias Klose <doko@debian.org>  Fri, 16 Jun 2006 22:02:26 +0000

python-scientific (2.4.9-4) unstable; urgency=low

  * python-scientific-doc: Install doc-base file (closes: #306514).
  * python-netcdf: Add dependency on python-scientific, needed by
    the Scientific/IO/NetCDF module (closes: #344090).
  * Rebuild to fix dependency on libnetcdf3 (closes: #352232).
  * Rebuild to fix dependency on lam4c2 (closes: #323414).

 -- Matthias Klose <doko@debian.org>  Sun, 12 Feb 2006 21:46:50 +0000

python-scientific (2.4.9-3) unstable; urgency=low

  * Rebuild for C++ ABI change.

 -- Matthias Klose <doko@debian.org>  Fri,  2 Sep 2005 20:16:53 +0000

python-scientific (2.4.9-2) unstable; urgency=low

  * python-mpi: Add dependency on python-scientific (thanks Yu Huang).

 -- Matthias Klose <doko@debian.org>  Sun,  3 Apr 2005 16:45:47 +0200

python-scientific (2.4.9-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Wed, 24 Nov 2004 07:18:34 +0100

python-scientific (2.4.6-2) unstable; urgency=low

  * Both mpichpython and lampython now provide 'mpipython' (closes: 268940).
  * Change priority of lampython to extra.
  * Update HTML docs (closes: #268256).

 -- Matthias Klose <doko@debian.org>  Tue, 21 Sep 2004 07:42:15 +0200

python-scientific (2.4.6-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Tue,  6 Jul 2004 01:07:07 +0200

python-scientific (2.4.5-2) unstable; urgency=low

  * lampython: depend on lam-runtime (closes: #236997).

 -- Matthias Klose <doko@debian.org>  Tue,  9 Mar 2004 21:00:34 +0100

python-scientific (2.4.5-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sat, 24 Jan 2004 20:46:43 +0100

python-scientific (2.4.3-4) unstable; urgency=low

  * Update package description (closes: #209940).

 -- Matthias Klose <doko@debian.org>  Fri, 26 Sep 2003 10:31:29 +0200

python-scientific (2.4.3-3) unstable; urgency=low

  * Upload for python2.3 as the default python version.
  * Add mpipython and lampython packages to handle both LAM based and
    MPICH based setups (closes: #196962).

 -- Matthias Klose <doko@debian.org>  Fri,  8 Aug 2003 09:22:49 +0200

python-scientific (2.4.3-2) unstable; urgency=low

  * Build python-mpi package (Michael Banck). Closes: #186923.

 -- Matthias Klose <doko@debian.org>  Fri, 18 Apr 2003 08:30:48 +0200

python-scientific (2.4.3-1.1) unstable; urgency=low

  * Added a mpipython package including
    - mpipython, as a replacement for the python executable
    - the Scientific/MPI modules
    - the mpi.py example file (formerly in the python-netcdf package)
  * Added appropriate -I flags to Src/MPI/compile.py
  * Remove stamp-build in debian/rules' clean-target

 -- Michael Banck <mbanck@debian.org>  Mon, 31 Mar 2003 04:50:21 +0200

python-scientific (2.4.3-1) unstable; urgency=low

  * New upstream version (closes: #179232).

 -- Matthias Klose <doko@debian.org>  Sun, 16 Mar 2003 15:17:06 +0100

python-scientific (2.4.1-1) unstable; urgency=low

  * New upstream version.
  * Use python2.2 (closes: #160650).
  * TODO: package BSPlib (http://www.bsp-worldwide.org/).

 -- Matthias Klose <doko@debian.org>  Thu, 12 Sep 2002 22:36:48 +0200

python-scientific (2.2-5) unstable; urgency=low

  * Add dependency to python-numeric-ext (closes: #127187).

 -- Matthias Klose <doko@debian.org>  Tue,  1 Jan 2002 18:01:32 +0100

python-scientific (2.2-4) unstable; urgency=low

  * Adapt dependencies to proposed python policy.

 -- Matthias Klose <doko@debian.org>  Sun, 11 Nov 2001 18:12:26 +0100

python-scientific (2.2-3) unstable; urgency=low

  * Only support one python version (2.1). Drop python2-scientific package.

 -- Matthias Klose <doko@debian.org>  Sat, 20 Oct 2001 18:39:57 +0200

python-scientific (2.2-2) unstable; urgency=low

  * Fix name of python2 interpreter in postinst (closes: #106860, #111183,
    #111186).

 -- Matthias Klose <doko@debian.org>  Sat,  8 Sep 2001 15:47:16 +0200

python-scientific (2.2-1) unstable; urgency=low

  * New upstream release.
  * No MPI support (yet).

 -- Matthias Klose <doko@debian.org>  Mon,  9 Apr 2001 18:29:16 +0200

python-scientific (2.0.1-2) unstable; urgency=low

  * Add build dependency (fixes #66392).

 -- Matthias Klose <doko@cs.tu-berlin.de>  Wed,  5 Jul 2000 18:13:40 +0200

python-scientific (2.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Mon, 26 Jun 2000 17:59:03 +0200

python-scientific (2.0-2) unstable; urgency=low

  * Import math in Statistics.__init__.py (fixes #62604).

 -- Matthias Klose <doko@cs.tu-berlin.de>  Thu,  4 May 2000 01:39:51 +0200

python-scientific (2.0-1) unstable; urgency=low

  * Final release.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Sat, 19 Feb 2000 13:21:58 +0100

python-scientific (2.0-0pre2) unstable; urgency=low

  * usr/doc -> usr/share/doc transition

 -- Matthias Klose <doko@cs.tu-berlin.de>  Fri,  7 Jan 2000 16:12:35 +0100

python-scientific (2.0-0pre1) frozen unstable; urgency=low

  * New upstream version ScientificPython 2.0b1 (include python-netcdf).

 -- Matthias Klose <doko@cs.tu-berlin.de>  Fri, 20 Aug 1999 20:51:39 +0200

python-netcdf (1.03-3) frozen unstable; urgency=low

  * Recompiled with new libc.
  * Make lintian happy.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Thu, 26 Nov 1998 22:40:11 +0200

python-netcdf (1.03-2) frozen unstable; urgency=low

  * Adopted python policy to install in /usr/lib/python1.5/site-packages.

 -- Matthias Klose <doko@debian.org>  Wed, 29 Apr 1998 12:30:25 +0200

python-netcdf (1.03-1) unstable; urgency=low

  * Maintainer change
  * new "upstream patch" fixes core dump when reading netCDF files

 -- Matthias Klose <doko@debian.org>  Fri, 20 Mar 1998 10:18:57 +0100

python-netcdf (1.0-2) unstable; urgency=low

  * Temporary maintainer change.

 -- Gregor Hoffleit <flight@debian.org>  Sun, 15 Mar 1998 12:44:37 +0100

python-netcdf (1.0-1) unstable; urgency=low

  * Initial Release.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Tue, 10 Mar 1998 12:43:50 +0100
